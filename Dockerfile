FROM mvijayaragavan/python3-testing:3.9.16-0
LABEL url="https://gitlab.com/microservices5894911/base_images/rejected-testing"
ARG REJECTED_VERSION=3.22.0
RUN pip3 install --upgrade rejected==${REJECTED_VERSION}
